<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-4.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
		</div>
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<article>

		<section>
			<div class="sw">
				<div class="article-body">
						
					<div class="hgroup centered">
						<h1 class="hgroup-title">Newsletter Sign Up</h1>
						<span class="hgroup-subtitle">We have your business covered.</span>
					</div><!-- .hgroup -->

					<p class="excerpt">
						Proin accumsan tellus vel placerat convallis. Ut mollis imperdiet laoreet. 
						Sed aliquet vehicula luctus. Mauris id tristique turpis, a convallis est. 
						Vestibulum dapibus luctus dolor, in rutrum sem auctor ultricies.
					</p>

					<br>

					<form action="/" class="body-form centered">
						<div class="fieldset grid collapse-700">

							<div class="col-1 col">
								<div class="item"><input name="fname" type="text" placeholder="First Name"></div>
							</div><!-- .col -->

							<div class="col-1 col">
								<div class="item"><input name="lname" type="text" placeholder="Last Name"></div>
							</div><!-- .col -->					

							<div class="col-1 col">
								<div class="item">
									<h4>Interests</h4>

									<label class="interest">
										<input type="checkbox">
										<span>Interest One</span>
									</label><!-- .interest -->

									<label class="interest">
										<input type="checkbox">
										<span>Interest Two</span>
									</label><!-- .interest -->

									<label class="interest">
										<input type="checkbox">
										<span>Interest Three</span>
									</label><!-- .interest -->

								</div>
							</div><!-- .col -->							

							<div class="col-1 col">
								<div class="item"><input name="email" type="email" placeholder="E-mail Address"></div>
							</div><!-- .col -->

						</div><!-- .grid -->
							<button class="button" type="submit">Send Message</button>				
					</form><!-- .body-form -->					

				</div><!-- .article-body -->
			</div><!-- .sw -->
		</section>
		
	</article>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>