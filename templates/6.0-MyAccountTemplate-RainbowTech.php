<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-5.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
		</div>
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
			<div class="article-body">
				<div class="hgroup centered">
					<h1 class="hgroup-title">My Account</h1>
					<span class="hgroup-subtitle">Keep your account up to date.</span>
				</div><!-- .hgroup -->

				<p class="excerpt">
					A well designed business card can create a lasting impression that will make a huge impact on your clients. 
					We can help you make that great first impression with high quality printing for any range of budgets. 
				</p>
			</div><!-- .article-body -->
		</div><!-- .sw -->
	</section>

	<section>
		<div class="sw">
			
			<div class="my-account">
				

				<div class="my-account-bar d-bg secondary-bg">
					<div>
						Welcome, <span class="name">username</span>
					</div>

					<div class="my-account-bar-actions">
						<a href="#" class="t-fa fa-pencil-square-o">Edit Your Information</a>
						<a href="#" class="t-fa fa-dollar">Edit Your Payment Information</a>
						<a href="#" class="t-fa fa-shopping-cart">View Your Cart</a>
					</div><!-- .my-account-bar-actions -->
				</div><!-- .my-account-bar -->


				<div class="my-account-mod">
					<h4>Your Information</h4>

					<div class="my-account-contact">

						<address>
							John Smith <br>
							123 Street Address <br>
							St. John's, NL A1B 2C3
						</address>

						<div class="rows">

							<div class="row">
								<span class="l secondary">Phone:</span>
								<span class="r">709.123.4567</span>
							</div><!-- .row -->

							<div class="row">
								<span class="l secondary">Email:</span>
								<span class="r">me@thisdomain.com</span>
							</div><!-- .row -->

						</div><!-- .rows -->

					</div><!-- .my-account-contact -->

				</div>

				<div class="my-account-mod">
					<h4>Active Orders</h4>

					<table class="my-account-order-table">
						<thead>
							<tr>
								<th>Order Number</th>
								<th>Date</th>
								<th>Status</th>
								<th>Amount</th>
								<th>Shipping</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td data-field="Order Number:">02929292</td>
								<td data-field="Date:">October 3, 2014</td>
								<td data-field="Status:">Processing</td>
								<td data-field="Amount:">$205.39</td>
								<td data-field="Shipping:">Standard</td>
							</tr>
							<tr>
								<td data-field="Order Number:">02929292</td>
								<td data-field="Date:">October 3, 2014</td>
								<td data-field="Status:">Processing</td>
								<td data-field="Amount:">$205.39</td>
								<td data-field="Shipping:">Standard</td>
							</tr>
						</tbody>
					</table>

				</div><!-- .my-account-mod -->

				<div class="my-account-mod">
					<h4>Order History</h4>

					<table class="my-account-order-table">
						<thead>
							<tr>
								<th>Order Number</th>
								<th>Date</th>
								<th>Status</th>
								<th>Amount</th>
								<th>Shipping</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td data-field="Order Number:">02929292</td>
								<td data-field="Date:">October 3, 2014</td>
								<td data-field="Status:">Processing</td>
								<td data-field="Amount:">$205.39</td>
								<td data-field="Shipping:">Standard</td>
							</tr>
							<tr>
								<td data-field="Order Number:">02929292</td>
								<td data-field="Date:">October 3, 2014</td>
								<td data-field="Status:">Processing</td>
								<td data-field="Amount:">$205.39</td>
								<td data-field="Shipping:">Standard</td>
							</tr>
							<tr>
								<td data-field="Order Number:">02929292</td>
								<td data-field="Date:">October 3, 2014</td>
								<td data-field="Status:">Processing</td>
								<td data-field="Amount:">$205.39</td>
								<td data-field="Shipping:">Standard</td>
							</tr>							
						</tbody>
					</table>

				</div><!-- .my-account-mod -->

			</div><!-- .my-account -->

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>