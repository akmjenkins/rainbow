<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
		</div>
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">


	<div class="section-nav">
		<div class="selector with-arrow">
			<select class="goto">
				<option value="http://link/to" selected>Business Cards</option>
				<option value="http://link/to">Stationary</option>
				<option value="http://link/to">Calendars</option>
				<option value="http://link/to">Labels &amp; Stickers</option>
				<option value="http://link/to">Copy &amp; Scanning</option>
			</select>
			<span class="value"></span>
		</div><!-- .selector -->		
		<ul>
			<li class="selected"><a href="#">Business Cards</a></li>
			<li><a href="#">Stationary</a></li>
			<li><a href="#">Calendars</a></li>
			<li><a href="#">Labels &amp; Stickers</a></li>
			<li><a href="#">Copy &amp; Scanning</a></li>
		</ul>
	</div><!-- .section-nav -->

	<section>
		<div class="article-body">
			<div class="sw">
				
				<div class="hgroup centered">
					<h1 class="hgroup-title">Business Cards</h1>
					<span class="hgroup-subtitle">We have your business covered.</span>
				</div><!-- .hgroup -->

				<p class="excerpt">
					A well designed business card can create a lasting impression that will make a huge impact on your clients. 
					We can help you make that great first impression with high quality printing for any range of budgets. 
				</p>

			</div><!-- .sw -->
		</div><!-- .article-body -->
	</section>

	<section>
		<div class="sw">
			
			<div class="order-form">
				
				<form class="order-form-body body-form full">
					
					<div class="order-form-section expanded">

						<div class="order-form-section-title d-bg secondary-bg">
							<h4 class="title">Colours</h4>
							<span>Select up to three colours.</span>
						</div><!-- .order-form-section-title -->

						<div class="order-form-section-content">
							
							<div class="grid">
								<div class="col-2-3 col">
									<div class="item">
										<h5>Standard Palette</h5>

										<div class="two-column colours">
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Yellow">
												<span>Yellow</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Wedgewood Blue">
												<span>Wedgewood Blue</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Warm Red">
												<span>Warm Red</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Process Blue">
												<span>Process Blue</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Rubine Red">
												<span>Rubine Red</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Amazon Green">
												<span>Amazon Green</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Rhodamine Red">
												<span>Rhodamine Red</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Green">
												<span>Green</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Purple">
												<span>Purple</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Chocolate Brown">
												<span>Chocolate Brown</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Violet">
												<span>Violet</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Cocoa Brown">
												<span>Cocoa Brown</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Navy Blue">
												<span>Navy Blue</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Process Cyan">
												<span>Process Cyan</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Reflex Blue">
												<span>Reflex Blue</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Process Magenta">
												<span>Process Magenta</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Fast Blue Lake">
												<span>Fast Blue Lake</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Process Yellow">
												<span>Process Yellow</span>
											</label>
											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="Process Black">
												<span>Process Black</span>
											</label>
										</div><!-- .colours -->

									</div>
								</div>
								<div class="col-1-3 col">
									<div class="item">
										<h5>Speciality Palette</h5>

										<div class="colours">

											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="873">
												<span>*873</span>
											</label>

											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="872">
												<span>*872</span>
											</label>

											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="871">
												<span>*871</span>
											</label>	

											<label class="colour" data-hex="#0f0f0f">
												<input type="checkbox" name="colour[]" value="877">
												<span>*877</span>
											</label>	

										</div><!-- .colours -->

										<small>* There is a slightly higher cost for metallic inks due to the extra cost of ink and cleaning the press</small>

									</div><!-- .item -->
								</div><!-- .col -->
							</div><!-- .grid -->

						</div><!-- .order-form-section-content -->

					</div><!-- .order-form-section -->

					<div class="order-form-section">

						<div class="order-form-section-title d-bg secondary-bg">
							<h4 class="title">Select Paper</h4>
							<span>Select a paper type for your business card.</span>
						</div><!-- .order-form-section-title -->

					</div><!-- .order-form-section -->

					<div class="order-form-section expanded">

						<div class="order-form-section-title d-bg secondary-bg">
							<h4 class="title">Order Details</h4>
							<span>Fill out the following information to create your order.</span>
						</div><!-- .order-form-section-title -->

						<div class="order-form-section-content">
							
							<h5>Shipping Details</h5>
							<div class="grid pad10">
								
							</div><!-- .grid -->

							<hr>

							<h5>Billing Details</h5>

							<div class="grid pad10">

								<div class="col col-2">
									<div class="item">
										<input type="text" name="billing_fname" placeholder="First name">
									</div>
								</div>

								<div class="col col-2">
									<div class="item">
										<input type="text" name="billing_fname" placeholder="First name">
									</div>
								</div>

							</div>

							<hr>

							<h5>Additional Information</h5>

							<input type="number" name="qty" placeholder="Quantity">
							<textarea name="more_details" placeholder="More Details"></textarea>
							<button class="button" type="submit">Submit</button>


						</div><!-- .order-form-section-content -->

					</div><!-- .order-form-section -->

				</form><!-- .order-form-body -->

				<div class="order-form-cart dark-bg">
					<h4>Order Summary</h4>

					<div class="cart-section">
						<div class="cart-section-title">Colour Selections</div>
						<span>Wedgewood Blue</span>
						<span>Chocolate Brown</span>
					</div><!-- .cart-section -->

					<div class="cart-section">
						<div class="cart-section-title">Paper Selections</div>
						<span>Bond Bright White 20</span>
					</div><!-- .cart-section -->

					<div class="cart-section">
						<div class="cart-section-title">Quantity</div>
						<span>100</span>
					</div><!-- .cart-section -->

					<div class="cart-section">
						<div class="cart-section-title">Cost</div>

						<div class="rows">
							<div class="row">
								<span class="l">Sub Total</span>
								<span class="r">$100.00</span>
							</div><!-- .row -->

							<div class="row">
								<span class="l">Tax</span>
								<span class="r">$10.00</span>
							</div><!-- .row -->

							<div class="row">
								<span class="l">Total</span>
								<span class="r">$110.00</span>
							</div><!-- .row -->							

						</div><!-- .rows -->

					</div><!-- .cart-section -->

					<button type="submit" class="button">Check Out</button>

				</div><!-- .order-form-cart -->
			</div><!-- .business-cards-order -->
	

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>