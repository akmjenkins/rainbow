<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav-wrap">

	<div class="nav sw">
		
		<nav>

			<ul>
				<li><a class="logo" href="#"><img src="../assets/dist/images/logos/rainbow-security-technologies-ltd.png" alt="Rainbow Security Technologies Ltd. Logo"></a></li>
				<li><a href="#">Security Printing</a></li>
			</ul>

			<ul>
				<li><a class="logo" href="#"><img src="../assets/dist/images/logos/rainbow-printing-ltd.png" alt="Rainbow Printing Ltd. Logo"></a></li>
				<li><a href="#">Commercial Printing</a></li>
				<li><a href="#">Business Cards</a></li>
				<li><a class="highlight" href="#">Request a Quote</a></li>
			</ul>

		</nav>

		
		<div class="nav-top">

			<div class="links">
				<a href="#">Blog</a>
				<a href="#">My Account</a>
				<a href="#">Get Help</a>
				<a href="#">Contact</a>				
			</div><!-- .links -->

			<?php include('i-social.php'); ?>

			<button class="toggle-search">Search</button>
			<button class="toggle-chat">Chat Now</button>
			
		</div><!-- .nav-top -->
		
	</div><!-- .nav -->

</div><!-- .nav-wrap -->