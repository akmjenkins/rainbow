<div class="order-block dark-bg">

	<div class="order-block-img">
		<div class="lazybg" data-src="../assets/dist/images/temp/order-img.jpg"></div>
	</div><!-- .img-wrap -->

	<div class="order-block-content">
		<span class="order-block-title">Order Your Custom Business Cards Today</span>
		<p>Choose from our large selection of colours and paper types to create a business card that expresses who your company is.</p>
	</div><!-- .order-block-content -->

	<div class="order-block-form">
		<p>Enter the following information to begin your business card order.</p>
		<form action="" class="body-form full">
			<div class="fieldset">
				<input type="text" name="name" placeholder="Name">
				<input type="email" name="email" placeholder="Email Address">
				<button class="button">Begin Your Order</button>
			</div><!-- .fieldset -->
		</form><!-- .body-form.full -->
	</div><!-- .order-block-form -->

</div><!-- .order-block -->