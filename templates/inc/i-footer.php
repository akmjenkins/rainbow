			<footer>

				<div class="sw">

					<div class="footer-blocks">
						
						<div class="footer-block">
							<ul class="footer-nav">
								<li><a href="#">Commercial Printing</a></li>
								<li><a href="#">Security Printing</a></li>
								<li><a href="#">Business Cards</a></li>
							</ul>
						</div><!-- .footer-block -->

						<div class="footer-block">
							
							<p>We can help your business with all it's printing needs.</p>
							<a href="#" class="button white">Request a Quote</a>

						</div><!-- .footer-block -->

						<div class="footer-block">
							
							<p>We can help your business with all it's printing needs.</p>
							
							<form action="/" class="single-form">
								<div class="fieldset">
									<input type="email" name="subscribe" placeholder="Enter Your E-mail Address">
									<button class="t-fa-abs fa-paper-plane-o">Subscribe</button>
								</div><!-- .fieldset -->
							</form><!-- .single-form -->

						</div><!-- .footer-block -->						

					</div><!-- .footer-blocks -->

					<div class="footer-contact">

						<address>
							3 Jones Court <br>
							Sussex NB, E4E 2S2
						</address>

						<span>Sussex <span>(505) 433-2877</span></span>
						<span>Fredericton <span>(506) 459-7981</span></span>
						<span>Saint John <span>(506) 633-1165</span></span>
						<span>Moncton <span>(877) 380-7462 (US/CAN)</span></span>

					</div><!-- .footer-contact -->

				</div><!-- .sw -->

				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Rainbow Security Technologies</a> All Rights Reserved.</li>
							<li><a href="#">About Us</a></li>
							<li><a href="#">Get Help</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
				
			</footer><!-- footer -->

			<div class="mobile-nav-bg"></div>

		</div><!-- .page-wrapper -->

		<?php include('i-search-form.php'); ?>

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/rainbow-gulp',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/dist/js/main.js"></script>
	</body>
</html>