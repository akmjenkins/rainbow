<div class="social">
	<a href="#" title="Like Rainbow Security Technologies on Facebook" class="social-fb" rel="external">Like Rainbow Security Technologies on Facebook</a>
	<a href="#" title="Follow Rainbow Security Technologies on Twitter" class="social-tw" rel="external">Follow Rainbow Security Technologies on Twitter</a>
	<a href="#" title="Add Rainbow Security Technologies to your Circles on Google Plus" class="social-gp" rel="external">Add Rainbow Security Technologies to your Circles on Google Plus</a>
	<a href="#" title="Subscribe to Rainbow Security Technologies' YouTube Channel" class="social-yt" rel="external">Subscribe to Rainbow Security Technologies' YouTube Channel</a>
	<a href="#" title="Connect with Rainbow Security Technologies on LinkedIn" class="social-in" rel="external">Connect with Rainbow Security Technologies on LinkedIn</a>
</div><!-- .social -->