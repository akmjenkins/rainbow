<?php

	//you can remove these, I only included it so I could simulate WordPress conditionals while I was building the template
	function is_home() {
		global $bodyclass;
		return preg_match('/home/',$bodyclass);
	}

	function is_404() {
		global $bodyclass;
		return preg_match('/error404/',$bodyclass);
	}
?>
<!doctype html>
<html lang="en">

	<head>
		<title>Rainbow</title>
		<meta charset="utf-8">
		
		<!-- jQuery -->
		<script src="../assets/dist/lib/jquery/dist/jquery.min.js"></script>
		
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,initial-scale=1.0">
		
		<!-- Open Sans -->
		<link href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700" rel="stylesheet">	

		<!-- slickslider -->
		<link rel="stylesheet" href="../assets/dist/lib/slick.js/slick/slick.css">
		<script src="../assets/dist/lib/slick.js/slick/slick.min.js"></script>
		
		<!-- magnificpopup -->
		<link rel="stylesheet" href="../assets/dist/lib/magnific-popup/dist/magnific-popup.css">
		<script src="../assets/dist/lib/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
		
		<link rel="stylesheet" href="../assets/dist/css/style.css?<?php echo time(); ?>">		
		
	</head>
	<body class="<?php echo $bodyclass; ?>">

		<!-- nav -->
		<?php include('i-nav.php'); ?>
		
		<header>
			<div class="sw">
				<a class="logo" href="#"><img src="../assets/dist/images/logos/rainbow-security-technologies-ltd.png" alt="Rainbow Security Technologies Ltd. Logo"></a>
				<a class="logo" href="#"><img src="../assets/dist/images/logos/rainbow-printing-ltd.png" alt="Rainbow Printing Ltd. Logo"></a>
			</div><!-- .sw -->
		</header>
	
		<div class="page-wrapper">	
			<div class="mobile-nav-bg"></div>