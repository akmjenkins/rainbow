<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
		</div>
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
		</div>
	</div><!-- .fader -->

	<div class="hero-swiper swiper-wrapper sw full">

		<div class="swiper" data-autoplay="true" data-autoplay-speed="5000">

			<div class="swipe-item">

				<div class="hero-swiper-content">
					<span class="h1-style">Guard Against Cheque Fraud</span>	
					<p>Our Tamper Proof cheques are designed with quality in mind, making it virtually impossible to duplicate, but easy to detect if tampered with.</p>
				</div><!-- .hero-swiper-content -->

				<a href="#" class="button">Read More</a>
				
			</div><!-- .swipe-item -->

			<div class="swipe-item">

				<div class="hero-swiper-content">
					<span class="h1-style">Slide #2</span>	
					<p>Our Tamper Proof cheques are designed with quality in mind, making it virtually impossible to duplicate, but easy to detect if tampered with.</p>
				</div><!-- .hero-swiper-content -->

				<a href="#" class="button">Read More</a>
				
			</div><!-- .swipe-item -->			

		</div><!-- .swiper -->

	</div><!-- .hero-swiper -->
	
</div><!-- .hero -->

<div class="body">

	<section class="light-bg">
		<div class="sw">
			
			<div class="tab-wrapper box-tabs">
				
				<div class="tab-controls">

					<div class="selector with-arrow">
						<select class="tab-controller">
							<option>Security Printing</option>
							<option>Commercial Printing</option>
						</select>
						<span class="value"></span>
					</div><!-- .selector -->
					

					<div class="tab-control selected">
						<img src="../assets/dist/images/logos/rainbow-security-technologies-ltd-white.png" alt="Rainbow Security Technologies Ltd. Logo">
						Security Printing
					</div><!-- .tab-control -->

					<div class="tab-control">
						<img src="../assets/dist/images/logos/rainbow-printing-ltd-white.png" alt="Rainbow Printing Ltd. Logo">
						Commercial Printing						
					</div><!-- .tab-control -->

				</div><!-- .tab-controls -->

				<div class="tab-holder">

					<div class="tab selected">

						<div class="box-grid grid eqh">

							<div class="col">
								<a class="item box" href="#">
									
									<div class="box-img-wrap">
										<div class="lazybg" data-src="../assets/dist/images/temp/block-1.jpg"></div>
									</div><!-- .box-img-wrap -->

									<div class="box-content">
										<div class="hgroup">
											<span class="h2-style hgroup-title">Prescription <br>RX Pads</span>
										</div><!-- .hgroup -->

										<span class="button">Read More</span>
									</div><!-- .box-content -->

								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col">
								<a class="item box" href="#">
									
									<div class="box-img-wrap">
										<div class="lazybg" data-src="../assets/dist/images/temp/block-2.jpg"></div>
									</div><!-- .box-img-wrap -->

									<div class="box-content">
										<div class="hgroup">
											<span class="h2-style hgroup-title">Secure <br> Cheques</span>
										</div><!-- .hgroup -->

										<span class="button">Read More</span>
									</div><!-- .box-content -->

								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col">
								<a class="item box" href="#">
									
									<div class="box-img-wrap">
										<div class="lazybg" data-src="../assets/dist/images/temp/block-3.jpg"></div>
									</div><!-- .box-img-wrap -->

									<div class="box-content">
										<div class="hgroup">
											<span class="h2-style hgroup-title">Tickets <br>&amp; Events</span>
										</div><!-- .hgroup -->

										<span class="button">Read More</span>
									</div><!-- .box-content -->

								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col">
								<a class="item box" href="#">
									
									<div class="box-img-wrap">
										<div class="lazybg" data-src="../assets/dist/images/temp/block-4.jpg"></div>
									</div><!-- .box-img-wrap -->

									<div class="box-content">
										<div class="hgroup">
											<span class="h2-style hgroup-title">Retail <br> Gifting</span>
										</div><!-- .hgroup -->

										<span class="button">Read More</span>
									</div><!-- .box-content -->

								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col">
								<a class="item box" href="#">
									
									<div class="box-img-wrap">
										<div class="lazybg" data-src="../assets/dist/images/temp/block-5.jpg"></div>
									</div><!-- .box-img-wrap -->

									<div class="box-content">
										<div class="hgroup">
											<span class="h2-style hgroup-title">ID &amp; Visitor <br> Management</span>
										</div><!-- .hgroup -->

										<span class="button">Read More</span>
									</div><!-- .box-content -->

								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col fill">
								<a class="item box box-info dark-bg" href="#">

									<div class="box-content">
										<p>
											Cheque and document Fraud losses range in billions of 
											dollars and are increasing at an alarming rate.
										</p>

										<span>Let us help protect your business.</span>

									</div><!-- .box-content -->

									<span class="button white">Request A Quote</span>									

								</a><!-- .item -->
							</div><!-- .col -->

						</div><!-- .box-grid -->

					</div><!-- .tab -->

					<div class="tab">

						<div class="box-grid grid eqh">

							<div class="col">
								<a class="item box" href="#">
									
									<div class="box-img-wrap">
										<div class="lazybg" data-src="../assets/dist/images/temp/block-3.jpg"></div>
									</div><!-- .box-img-wrap -->

									<div class="box-content">
										<div class="hgroup">
											<span class="h2-style hgroup-title">Tickets <br>&amp; Events</span>
										</div><!-- .hgroup -->

										<span class="button">Read More</span>
									</div><!-- .box-content -->

								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col">
								<a class="item box" href="#">
									
									<div class="box-img-wrap">
										<div class="lazybg" data-src="../assets/dist/images/temp/block-4.jpg"></div>
									</div><!-- .box-img-wrap -->

									<div class="box-content">
										<div class="hgroup">
											<span class="h2-style hgroup-title">Retail <br> Gifting</span>
										</div><!-- .hgroup -->

										<span class="button">Read More</span>
									</div><!-- .box-content -->

								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col">
								<a class="item box" href="#">
									
									<div class="box-img-wrap">
										<div class="lazybg" data-src="../assets/dist/images/temp/block-1.jpg"></div>
									</div><!-- .box-img-wrap -->

									<div class="box-content">
										<div class="hgroup">
											<span class="h2-style hgroup-title">Prescription <br>RX Pads</span>
										</div><!-- .hgroup -->

										<span class="button">Read More</span>
									</div><!-- .box-content -->

								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col">
								<a class="item box" href="#">
									
									<div class="box-img-wrap">
										<div class="lazybg" data-src="../assets/dist/images/temp/block-2.jpg"></div>
									</div><!-- .box-img-wrap -->

									<div class="box-content">
										<div class="hgroup">
											<span class="h2-style hgroup-title">Secure <br> Cheques</span>
										</div><!-- .hgroup -->

										<span class="button">Read More</span>
									</div><!-- .box-content -->

								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col">
								<a class="item box" href="#">
									
									<div class="box-img-wrap">
										<div class="lazybg" data-src="../assets/dist/images/temp/block-5.jpg"></div>
									</div><!-- .box-img-wrap -->

									<div class="box-content">
										<div class="hgroup">
											<span class="h2-style hgroup-title">ID &amp; Visitor <br> Management</span>
										</div><!-- .hgroup -->

										<span class="button">Read More</span>
									</div><!-- .box-content -->

								</a><!-- .item -->
							</div><!-- .col -->

							<div class="col fill">
								<a class="item box box-info dark-bg" href="#">

									<div class="box-content">
										<p>
											Cheque and document Fraud losses range in billions of 
											dollars and are increasing at an alarming rate.
										</p>

										<span>Let us help protect your business.</span>

									</div><!-- .box-content -->

									<span class="button white">Request A Quote</span>									

								</a><!-- .item -->
							</div><!-- .col -->

						</div><!-- .box-grid -->
						
					</div><!-- .tab -->

				</div><!-- .tab-holder -->

			</div><!-- .tab-wrapper -->

		</div><!-- .sw -->


		<div class="sw">
			<?php include('inc/i-order-block.php'); ?>
		</div><!-- .sw -->
		
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>