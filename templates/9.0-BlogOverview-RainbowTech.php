<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
		</div>
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<div class="light-bg">

		<section>
			<div class="sw">
				
				<div class="hgroup centered">
					<h1 class="hgroup-title">Blog</h1>
					<span class="hgroup-subtitle">Stay Connected to our Business</span>
				</div><!-- .hgroup -->

			</div>
		</section>

		<section class="filter-section">
			<div class="sw">
				
				<div class="filter-bar">
					<div class="filter-bar-content">

						<div class="filter-bar-left">
							<div class="count">
								<span class="num">10</span> Posts Found
							</div><!-- .count -->
						</div><!-- .filter-bar-left -->

						<div class="filter-bar-meta">
						
							<div class="filter-controls">
								<button class="previous">Prev</button>
								<button class="next">Next</button>
							</div><!-- .filter-controls -->
					
						</div><!-- .filter-bar-meta -->

					</div><!-- .filter-bar-content -->
				</div><!-- .filter-bar -->

				<div class="filter-content">
					
					<div class="grid eqh blocks collapse-at-850 blocks">
						
						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button bounce" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/block-6.jpg"></div>
									</div><!-- .img-wrap -->

									<div class="content">
									
										<div class="hgroup">
											<h2 class="hgroup-title">Blog Post Title</h2>
										</div><!-- .hgroup -->

										<time datetime="2015-02-12">February 12, 2015</time>
										
										<p>
											Fusce convallis aliquam sem sit amet laoreet. Nulla lobortis viverra viverra. 
											Sed nec varius dolor. Nulla sagittis nulla vel lorem bibendum egestas.
										</p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button bounce" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/block-6.jpg"></div>
									</div><!-- .img-wrap -->

									<div class="content">
									
										<div class="hgroup">
											<h2 class="hgroup-title">Blog Post Title</h2>
										</div><!-- .hgroup -->

										<time datetime="2015-02-12">February 12, 2015</time>
										
										<p>
											Fusce convallis aliquam sem sit amet laoreet. Nulla lobortis viverra viverra. 
											Sed nec varius dolor. Nulla sagittis nulla vel lorem bibendum egestas.
										</p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->	

						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button bounce" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/block-6.jpg"></div>
									</div><!-- .img-wrap -->

									<div class="content">
									
										<div class="hgroup">
											<h2 class="hgroup-title">Blog Post Title</h2>
										</div><!-- .hgroup -->

										<time datetime="2015-02-12">February 12, 2015</time>
										
										<p>
											Fusce convallis aliquam sem sit amet laoreet. Nulla lobortis viverra viverra. 
											Sed nec varius dolor. Nulla sagittis nulla vel lorem bibendum egestas.
										</p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->	

						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button bounce" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/block-6.jpg"></div>
									</div><!-- .img-wrap -->

									<div class="content">
									
										<div class="hgroup">
											<h2 class="hgroup-title">Blog Post Title</h2>
										</div><!-- .hgroup -->

										<time datetime="2015-02-12">February 12, 2015</time>
										
										<p>
											Fusce convallis aliquam sem sit amet laoreet. Nulla lobortis viverra viverra. 
											Sed nec varius dolor. Nulla sagittis nulla vel lorem bibendum egestas.
										</p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->	

						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button bounce" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/block-6.jpg"></div>
									</div><!-- .img-wrap -->

									<div class="content">
									
										<div class="hgroup">
											<h2 class="hgroup-title">Blog Post Title</h2>
										</div><!-- .hgroup -->

										<time datetime="2015-02-12">February 12, 2015</time>
										
										<p>
											Fusce convallis aliquam sem sit amet laoreet. Nulla lobortis viverra viverra. 
											Sed nec varius dolor. Nulla sagittis nulla vel lorem bibendum egestas.
										</p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->	

						<div class="col-3 col">
							<div class="item">
							
								<a class="block with-button bounce" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/block-6.jpg"></div>
									</div><!-- .img-wrap -->

									<div class="content">
									
										<div class="hgroup">
											<h2 class="hgroup-title">Blog Post Title</h2>
										</div><!-- .hgroup -->

										<time datetime="2015-02-12">February 12, 2015</time>
										
										<p>
											Fusce convallis aliquam sem sit amet laoreet. Nulla lobortis viverra viverra. 
											Sed nec varius dolor. Nulla sagittis nulla vel lorem bibendum egestas.
										</p>
										
										<span class="button">Read More</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->				
						
					</div><!-- .grid -->

				</div><!-- .filter-content -->

			</div><!-- .sw -->
		</section>

	</div><!-- .light-bg -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>