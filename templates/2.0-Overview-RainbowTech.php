<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
		</div>
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section class="light-bg">
		<div class="sw">
			
			<div class="hgroup centered">
				<h1 class="hgroup-title">Commercial Printing</h1>
				<span class="hgroup-subtitle">We have your business covered.</span>
			</div><!-- .hgroup.centered -->

			<div class="box-grid grid eqh">

				<div class="col">
					<a class="item box" href="#">
						
						<div class="box-img-wrap">
							<div class="lazybg" data-src="../assets/dist/images/temp/block-6.jpg"></div>
						</div><!-- .box-img-wrap -->

						<div class="box-content">
							<div class="hgroup">
								<span class="h2-style hgroup-title">Business <br> Cards</span>
							</div><!-- .hgroup -->

							<span class="button">Read More</span>
						</div><!-- .box-content -->

					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col">
					<a class="item box" href="#">
						
						<div class="box-img-wrap">
							<div class="lazybg" data-src="../assets/dist/images/temp/block-7.jpg"></div>
						</div><!-- .box-img-wrap -->

						<div class="box-content">
							<div class="hgroup">
								<span class="h2-style hgroup-title">Stationary</span>
							</div><!-- .hgroup -->

							<span class="button">Read More</span>
						</div><!-- .box-content -->

					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col">
					<a class="item box" href="#">
						
						<div class="box-img-wrap">
							<div class="lazybg" data-src="../assets/dist/images/temp/block-8.jpg"></div>
						</div><!-- .box-img-wrap -->

						<div class="box-content">
							<div class="hgroup">
								<span class="h2-style hgroup-title">Calendars</span>
							</div><!-- .hgroup -->

							<span class="button">Read More</span>
						</div><!-- .box-content -->

					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col">
					<a class="item box" href="#">
						
						<div class="box-img-wrap">
							<div class="lazybg" data-src="../assets/dist/images/temp/block-9.jpg"></div>
						</div><!-- .box-img-wrap -->

						<div class="box-content">
							<div class="hgroup">
								<span class="h2-style hgroup-title">Labels <br> &amp; Stickers</span>
							</div><!-- .hgroup -->

							<span class="button">Read More</span>
						</div><!-- .box-content -->

					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col">
					<a class="item box" href="#">
						
						<div class="box-img-wrap">
							<div class="lazybg" data-src="../assets/dist/images/temp/block-10.jpg"></div>
						</div><!-- .box-img-wrap -->

						<div class="box-content">
							<div class="hgroup">
								<span class="h2-style hgroup-title">Copy <br> &amp; Scanning</span>
							</div><!-- .hgroup -->

							<span class="button">Read More</span>
						</div><!-- .box-content -->

					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col fill">
					<a class="item box box-info dark-bg" href="#">

						<div class="box-content">
							<p>
								Cheque and document Fraud losses range in billions of 
								dollars and are increasing at an alarming rate.
							</p>

							<span>Let us help protect your business.</span>

						</div><!-- .box-content -->

						<span class="button white">Request A Quote</span>									

					</a><!-- .item -->
				</div><!-- .col -->

			</div><!-- .box-grid -->

		</div><!-- .sw -->
		
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>