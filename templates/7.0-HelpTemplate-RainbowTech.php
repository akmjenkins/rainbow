<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
		</div>
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
			<div class="article-body">
				<div class="hgroup centered">
					<h1 class="hgroup-title">Get Help</h1>
					<span class="hgroup-subtitle">We are here to help.</span>
				</div><!-- .hgroup -->

				<p class="excerpt">
					A well designed business card can create a lasting impression that will make a huge impact on your clients. 
					We can help you make that great first impression with high quality printing for any range of budgets. 
				</p>
			</div><!-- .article-body -->

			<div class="help-mod secondary-bg d-bg">

				<div class="help-mod-content">
					<span class="help-mod-title">How Can We Help?</span>
					<p>Type your question, keyword or phrase into our search box to quickly find what you are looking for.</p>
				</div><!-- .help-mod-content -->

				<div class="help-mod-form">
					<form action="" class="single-form">
						<div class="fieldset">
							
							<span class="count">15</span>

							<input type="text" name="s" placeholder="Start your search...">
							<button class="t-fa-abs fa-search">Search</button>

						</div><!-- .fieldset -->
					</form>	<!-- .single-form -->
				</div><!-- .help-mod-form -->

			</div><!-- .help-mod -->

		</div><!-- .sw -->
	</section>

	<section>
		<div class="sw">
			
			<div class="hgroup centered">
				<h1 class="hgroup-title">FAQ'S</h1>
				<span class="hgroup-subtitle">What are people asking?</span>
			</div><!-- .hgroup -->

			<div class="main-body">
				<div class="content">
					
					<div class="faq-mod">

						<div class="acc with-indicators separated allow-multiple">

							<div class="acc-item">
								<div class="acc-item-handle">Praesent consectetur augue leo, quis ultricies orci porta ut?</div>
								<div class="acc-item-content">
									<p>
										Maecenas arcu ipsum, dignissim eu consectetur eu, interdum non risus. Donec quam turpis, venenatis ut posuere a, pretium eu nibh. Sed in vestibulum magna, et malesuada erat.									
									</p>
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->

							<div class="acc-item">
								<div class="acc-item-handle">Praesent consectetur augue leo, quis ultricies orci porta ut?</div>
								<div class="acc-item-content">
									<p>
										Maecenas arcu ipsum, dignissim eu consectetur eu, interdum non risus. Donec quam turpis, venenatis ut posuere a, pretium eu nibh. Sed in vestibulum magna, et malesuada erat.									
									</p>
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->

							<div class="acc-item">
								<div class="acc-item-handle">Praesent consectetur augue leo, quis ultricies orci porta ut?</div>
								<div class="acc-item-content">
									<p>
										Maecenas arcu ipsum, dignissim eu consectetur eu, interdum non risus. Donec quam turpis, venenatis ut posuere a, pretium eu nibh. Sed in vestibulum magna, et malesuada erat.									
									</p>
								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->

						</div><!-- .acc -->

					</div><!-- .faq-mod -->

				</div><!-- .content -->
				<aside class="sidebar">
					<?php include('inc/i-contact-mod.php'); ?>
				</aside><!-- .sidebar -->
			</div><!-- .main-body -->

		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>