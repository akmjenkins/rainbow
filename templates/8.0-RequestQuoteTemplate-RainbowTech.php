<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
		</div>
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<article>

		<section>
			<div class="sw">
				<div class="article-body">
					
					<div class="hgroup centered">
						<h1 class="hgroup-title">Contact Us</h1>
						<span class="hgroup-subtitle">We have your business covered.</span>
					</div><!-- .hgroup -->

					<p class="excerpt">
						Proin accumsan tellus vel placerat convallis. Ut mollis imperdiet laoreet. 
						Sed aliquet vehicula luctus. Mauris id tristique turpis, a convallis est. 
						Vestibulum dapibus luctus dolor, in rutrum sem auctor ultricies.
					</p>				

				</div><!-- .article-body -->
				
			</div><!-- .sw -->

			<br>

			<div class="sw">

					<div class="grid pad40 collapse-800">
						<div class="col-2 col">
							<div class="item">
								<form action="/" class="body-form full">
									<fieldset>
										
										<input type="text" name="name" placeholder="Your Name">
										<input type="email" name="email" placeholder="Your Email">
										
										<textarea name="message" cols="30" rows="10" placeholder="Your Message"></textarea>
										
										<button type="submit" class="button fill">Submit</button>
										
									</fieldset>
								</form>
							</div><!-- .item -->
						</div><!-- .col -->
						<div class="col-2 col">
							<div class="item">

								<div class="mod-contact-us">
							
									<div class="swiper-wrapper">
									
										<div class="selector with-arrow">
											<select class="swiper-nav">
												<option>Location One</option>
												<option>Location Two</option>
												<option>Location Three</option>
											</select>
											
											<span class="value">&nbsp;</span>
										</div><!-- .selector -->
									
										<div class="swiper" data-arrows="false">
											<div class="swipe-item">

												<div class="address-phones">
													<address>
														3 Jones Court <br>
														Sussex, NB E4E 2S2
													</address>											

													<div class="phones">
														<span class="block">P 1 506 433 2877</span>
														<span class="block">TF 1 877 380 7642</span>
													</div>
												</div><!-- .address-phones -->
												
												<div class="ar" data-ar="50">
													<iframe class="ar-child" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2785.098834353377!2d-65.4825346!3d45.729112699999995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4ca722c2b6c739d7%3A0x842cb9d7fb340fd5!2s3+Jones+Ct%2C+Sussex%2C+NB+E4E!5e0!3m2!1sen!2sca!4v1426617302189" frameborder="0" style="border:0"></iframe>
												</div><!-- .ar -->
											
											</div><!-- .swipe-item -->
											
											<div class="swipe-item">

												<div class="address-phones">
													<address>
														3 Jones Court <br>
														Sussex, NB E4E 2S2
													</address>											

													<div class="phones">
														<span class="block">P 1 506 433 2877</span>
														<span class="block">TF 1 877 380 7642</span>
													</div>
												</div><!-- .address-phones -->
												
												<div class="ar" data-ar="50">
													<iframe class="ar-child" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2785.098834353377!2d-65.4825346!3d45.729112699999995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4ca722c2b6c739d7%3A0x842cb9d7fb340fd5!2s3+Jones+Ct%2C+Sussex%2C+NB+E4E!5e0!3m2!1sen!2sca!4v1426617302189" frameborder="0" style="border:0"></iframe>
												</div><!-- .ar -->
											
											</div><!-- .swipe-item -->

											<div class="swipe-item">

												<div class="address-phones">
													<address>
														3 Jones Court <br>
														Sussex, NB E4E 2S2
													</address>											

													<div class="phones">
														<span class="block">P 1 506 433 2877</span>
														<span class="block">TF 1 877 380 7642</span>
													</div>
												</div><!-- .address-phones -->
												
												<div class="ar" data-ar="50">
													<iframe class="ar-child" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2785.098834353377!2d-65.4825346!3d45.729112699999995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4ca722c2b6c739d7%3A0x842cb9d7fb340fd5!2s3+Jones+Ct%2C+Sussex%2C+NB+E4E!5e0!3m2!1sen!2sca!4v1426617302189" frameborder="0" style="border:0"></iframe>
												</div><!-- .ar -->
											
											</div><!-- .swipe-item -->

										</div><!-- .swiper -->
									</div><!-- .swiper-wrapper -->

								</div><!-- .mod-contact-us -->
								
							</div><!-- .item -->
						</div><!-- .col -->
					</div><!-- .grid -->

			</div><!-- .sw -->

		</section>
		
	</article>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>